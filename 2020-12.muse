#title <lisp>year</lisp>年<lisp>month</lisp>月 東京エリア・関西合同Debian勉強会（オンライン開催）
<lisp>(progn (setq year 2020) (setq month 12) (setq day 20) (setq number 192) (setq eventid 196799) nil) </lisp>
* 開催概要

日時 | <lisp>year</lisp>年<lisp>month</lisp>月<lisp>day</lisp>日(日曜日) 14:00-17:00
参加登録方法 | [[https://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass上での案内]]の掲示を見て参加申し込みをお願いします。<br>
開催方法 | Google Meetを用いたオンラインのビデオ会議で行う予定です。インターネット回線がある場所からご参加ください。ビデオ会議の参加の方は[[https://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass上での案内]]をご参照ください。

* 当日のagenda

時間 || 内容 || 概要 || 発表者
14:00 |  ビデオ会議のURLに集合 |  | 参加者全員
14:00-14:10 | 開会、説明事項の連絡 |  | dictossさん
14:10-14:25 | 自己紹介、事前課題発表 |  | 参加者全員
14:25-14:45 | セミナー「Debian勉強会の資料の作成方法について」 | ・[[https://salsa.debian.org/tokyodebian-team/monthly-report][Debian勉強会の原稿資料はgitで管理]]しており、誰でもソースコードのtexファイル等を見ることができ、またビルドできるようになっています。<br>・今年改善した勉強会資料の仕組みの説明とそれを踏まえた勉強会資料の作成方法についてお話します。 | 上川さん
14:45-14:50 | 休憩 |  |
14:50-15:30 | BoF「2020年の振り返りと来年の目標」 | ・今回は2020年最後のDebian勉強会です。<br>・2020年はCOVID-19の感染を回避するためオンラインで勉強会をする試みが進んだ年でした。<br>・今年取り組みができてよかったこと、改善するとよいこと、来年に向けた目標を参加者みなさんと一緒に整理してみたいと思います。 | 参加者全員
15:30-15:40 | 休憩 |  |
15:40-16:10 | セミナー「実演：レガシーなDebianパッケージをモダンにしてみる」前半 | 現在、Debianのリポジトリにはdebugパッケージも含めると[[https://packages.debian.org/unstable/allpackages][137,411に及ぶバイナリパッケージ]]が存在しています。しかし、すべてが同じレベルでケアされているわけではなく、残念ながら一部メンテナンスが滞っているパッケージが存在しているのも事実です。そんなレガシーな状態のパッケージをリファクタリング＋αし、モダンな状態にするにはどうしたらよいでしょうか？<br>今回は何故レガシーパッケージをモダンにしたいのか？という点を説明し、その後に **どのような形でメンテナンスを行うか、というのを実演という形でお見せしたい** と思います。まとまった資料は時間の都合上用意できないため、是非各自でメモをとってBlogやSNS等で何を学んだのかを積極的に発信下さい。<br>既存パッケージをbetterな形にする方法を学んで、contributionに踏み出してみましょう。 | やまねひでき さん
16:10-16:15 | 休憩 |  |
16:15-16:55 | セミナー「実演：レガシーなDebianパッケージをモダンにしてみる」後半 | （同上） | やまねひでき さん
16:55-17:00 | 閉会、諸連絡 |  | dictossさん
17:00 | 解散 |  |

* 勉強会の申し込みと詳細情報

 勉強会の申し込みは イベント管理サービスである [[http://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass]]に掲示しています。詳細はリンク先を参照ください。

* 資料

 - [[./pdf<lisp>year</lisp>/debianmeetingresume<lisp>(format "%.4d%.2d" year month)</lisp>.pdf][PDF事前配布資料]]
 - [[./pdf<lisp>year</lisp>/debianmeetingresume<lisp>(format "%.4d%.2d" year month)</lisp>-presentation.pdf][PDF司会進行資料]]
 - [[https://docs.google.com/presentation/d/1bD6k73UYxm-j6W2x8KNJrt-oziPGF3XFLuKg_7NweXE/edit?usp=sharing][プレゼンテーション資料「Why we should make legacy debian packages modern?」（やまねひできさん）]]
 - [[/2020-12_tokyodebian_bof.txt][セミナーのディスカッション議事録]]

* 参加報告

