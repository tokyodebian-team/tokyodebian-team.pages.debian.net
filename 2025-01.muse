#title <lisp>year</lisp>年<lisp>month</lisp>月 東京エリア・関西合同Debian勉強会（オンライン開催）
<lisp>(progn (setq year 2025) (setq month 1) (setq day 18) (setq number 241) (setq eventid 340916) nil) </lisp>
* 開催概要

日時 | <lisp>year</lisp>年<lisp>month</lisp>月<lisp>day</lisp>日(土曜日) 14:00-16:00
参加登録方法 | [[https://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass上での案内]]の掲示を見て参加申し込みをお願いします。<br>
開催方法 | インターネットを用いたオンラインのビデオ会議で行う予定です。インターネット回線がある場所からご参加ください。ビデオ会議の参加の方は[[https://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass上での案内]]をご参照ください。

* 当日のagenda



時間 || 内容 || 概要 || 発表者
14:00 | ビデオ会議のURLに集合 |  | 参加者全員
14:00-14:05 | 開会、説明事項の連絡 |  | dictoss さん
14:05-14:15 | 自己紹介、事前課題発表 |  | 参加者全員
14:15-15:00 | セミナー「Debian 12 bookwormでtrac-1.6を構築してみた」 | ・[[https://groups.google.com/g/trac-users/c/8Oo_Bvru6Og][tracの安定版であるtrac-1.6が2023/09/24にリリースされています]] 。<br>・tracはこれまで安定版のtrac-1.4系が出ていましたがこれはpython2.7までの対応で、trac-1.6で初めてpython3系に移行しました。<br>・自宅で使っているtracサーバのバ―ジョンアップを検討しており、Debian 12 bookwormへtrac-1.6をインストールしてみました。<br>・本発表ではtracに関する基礎情報、インストール手順を説明します。 | dictoss
15:00-15:10 | 休憩 |  |
15:10-15:55 | BoF「2025 年の目標」 | ・今回は2025年最初のDebian勉強会です。<br>・今年一年の各自の目標やDebianにどうなってほしいかなどの思いを参加者皆さんで共有したいと思います。 | 参加者全員
15:55-16:00 | 閉会、諸連絡 |  | dictoss さん
16:00 | 解散 |  |

* 事前課題

 - BoF「2025 年の目標」の個人目標を当日までに考えてみてください。

* 勉強会の申し込みと詳細情報

 - 勉強会の申し込みは イベント管理サービスである [[http://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass]] に掲示しています。詳細はリンク先を参照ください。

* 資料

 - [[./pdf<lisp>year</lisp>/debianmeetingresume<lisp>(format "%.4d%.2d" year month)</lisp>.pdf][PDF事前配布資料]]
 - [[./pdf<lisp>year</lisp>/debianmeetingresume<lisp>(format "%.4d%.2d" year month)</lisp>-presentation.pdf][PDF司会進行資料]]
 - [[/2025-01_tokyodebian_bof.txt][セミナーのディスカッション議事録]]

* 参加報告

