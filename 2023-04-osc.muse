#title <lisp>year</lisp>年<lisp>month</lisp>月 OSC <lisp>year</lisp> Tokyo/Spring 出張展示
<lisp>(progn (setq year 2023) (setq month 4) (setq day 1) (setq number "") (setq eventid "") "") </lisp>
* 開催概要

東京エリアDebian勉強会は、OSC <lisp>year</lisp> Tokyo/Spring に参加します。

日時    | <lisp>year</lisp>年<lisp>month</lisp>月<lisp>day</lisp>日(土曜日) 10:00～16:00
会場案内 | [[https://event.ospn.jp/osc2023-spring/][東京都立産業貿易センター台東館 7階]]

* 当日のagenda

時間 || 内容 || 概要 || 発表者
<lisp>month</lisp>/<lisp>day</lisp>(土) 10:00-16:00 | Debianに関する展示 | Debian インストールマシンの展示、勉強会資料の展示、グッズの頒布 | 杉本

 - 注意：本イベントはセミナー発表はなく、展示のみです。

* 懇親会

なし

* 資料


* 参加報告
