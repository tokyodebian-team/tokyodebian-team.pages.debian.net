#!/bin/bash

docker run -t tokyodebian/debianmeeting-web:12 \
sh -c "git clone https://salsa.debian.org/tokyodebian-team/tokyodebian-team.pages.debian.net.git && \
cd tokyodebian-team.pages.debian.net && make"
