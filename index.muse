#title 東京エリアDebian勉強会

* 背景

2005年当初、東京近辺で、類似の勉強会は存在していませんでした。Debian について語る場所を提供するため、
Debian 勉強会を開催します。

この勉強会では参加の条件として事前課題を設定することがありますのでその場合は提出をお願いいたします。
また、勉強会の事前準備があるため、事前に参加登録するようお願いいたします。

現在、 Debian 勉強会は [[https://www.debian.or.jp/][Debian JP Project]]
の後援を受けてイベントを運営しています。

* 次回の勉強会

 - [[2025-03][2025年3月 東京エリア・関西合同Debian勉強会（オンライン開催）]]

* 未来の勉強会

 - [[2025-04][2025年4月 東京エリア・関西合同Debian勉強会（オンライン開催）]]

* 過去の勉強会

 - [[2025-02-osc][2025年2月 OSC 2025 Tokyo/Spring 出張勉強会（2025年2月 東京エリアDebian勉強会）]] セミナー「Debian Updates」
 - [[2025-01][2025年1月 東京エリア・関西合同Debian勉強会（オンライン開催）]] セミナー「Debian 12 bookwormでtrac-1.6を構築してみた」/BoF「2025 年の目標」
 - [[2024-12][2024年12月 東京エリア・関西合同Debian勉強会（オンライン開催）]] BoF「2024 年を振り返って」
 - [[2024-11][2024年11月 東京エリア・関西合同Debian勉強会（オンライン開催）]] セミナー「より新しい Mozc を Debian でも使いたい」/ミーティング「DebConf26 日本招致準備会議 #1」
 - [[2024-10-osc][2024年10月 OSC 2024 Tokyo/Fall 出張展示]]
 - [[2024-10-osc-online][2024年10月 OSC 2024 Online/Fall 出張勉強会（2024年10月 東京エリアDebian勉強会）]] セミナー「Debian Updates」
 - [[2024-09][2024年9月 東京エリア・関西合同Debian勉強会（現地・オンライン同時開催）]] BoF「DebConf26 日本開催に関する意見交換」/「Debian/OSSよろず相談・作業会」
 - [[2024-08][2024年8月 東京エリア・関西合同Debian勉強会（オンライン開催）]] セミナー「WSL2 での sid 生活」/情報交換会「DebConf24 イベント報告」
 - [[https://debconf24.debconf.org/][DebConf24 in 韓国・釜山]]
 - [[2024-07][2024年7月 東京エリア・関西合同Debian勉強会（オンライン開催）]] セミナー「General Resolution:tag2upload は Debian にどんな変化をもたらすのか」/情報交換会「DebConf24 直前特集」

** 2024年以前

 - [[past-event][2024年以前の勉強会記録]]

* 資料集

 - [[undocumenteddebian][あんどきゅめんてっどでびあん 過去PDF]]

* 連絡先

 - ウェブマスター: 杉本典充 dictoss@debian.or.jp
 - 日本語での相談先: [[https://www.debian.or.jp/community/ml/openml.html][debian-devel@lists.debian.or.jp]]

* 勉強会資料提出方法

発表したい方は [[prework-update][発表資料提出方法]]を元に原稿を作成して下さい。Gitのワークフロー紹介の例としてもご参照ください。

* 関連記事など

 - 2008/2/18 ＠IT自分戦略研究所 [[http://jibun.atmarkit.co.jp/lcom01/special/semi/semi01.html][先達を見つけ、自分なりの勉強会を開催せよ]]
 - 2007/12/27 ＠IT自分戦略研究所 [[http://jibun.atmarkit.co.jp/lcom01/special/comeve01/comeve01.html][積極的な発言が勉強会を盛り上げる]]

* リンクなど

 - [[./pdf2007/debianmeetingresume200712.pdf][資料の編集方針など（2007年12月用資料）]]
 - [[./pdf2018/debianmeetingresume201806.pdf][勉強会のWeb/原稿システムの仕組み（2018年6月用資料）]]
 - [[https://wiki.debian.org/KansaiDebianMeeting][Kansai Debian Meeting]]
   - 関西エリアで行っているDebian勉強会のページです。関西方面の方はこちらに参加してみてはいかがでしょうか。
 - Cross Distro Developers Camp
   - Debianを含めたディストリビューション共通の課題を解決することを目的とした開発者の集まりです。
   - 発表資料
     - [[https://event.ospn.jp/odc2024/article/odc24_cddc][オープンデベロッパーズカンファレンス 2024 発表スライド（Linux ディストリビューション開発の近況）]]
     - [[https://event.ospn.jp/odc2023/session/1080690][オープンデベロッパーズカンファレンス 2023 発表スライド（Linux ディストリビューション開発の近況）]]
     - [[https://event.ospn.jp/odc2022-online/session/649126][オープンデベロッパーズカンファレンス 2022 Online発表スライド（開発の近況 Debian 編）]]
     - [[https://event.ospn.jp/odc2021-online/session/392935][オープンデベロッパーズカンファレンス 2021 Online発表スライド（Debian 11 bullseyeとリリースモデル）]]
     - [[https://speakerdeck.com/dictoss/debianfalsegoshao-jie-open-developers-conference-2020-online][オープンデベロッパーズカンファレンス 2020 Online発表スライド（Debianのご紹介）]]
     - [[https://www.ospn.jp/odc2019/pdf/ODC2019_cddc.pdf][オープンデベロッパーズカンファレンス 2019 Tokyo発表スライド]]

* 運営向けの情報

 - google group上で行っています。勉強会運営に興味がある方は ウェブマスター、勉強会運営メンバー または [[https://www.debian.or.jp/community/ml/openml.html][debian-devel@lists.debian.or.jp]] に連絡ください。
 - [[https://calendar.google.com/calendar/u/0/embed?src=rtf297971l0d16vifcuagmhjgc@group.calendar.google.com&ctz=Asia/Tokyo][Google カレンダー（Debian JP）]]
 - [[editing][このページの編集方法]]
 - [[how-to-hosting-an-event][イベント開催準備の流れ]]
 - [[https://salsa.debian.org/tokyodebian-team/][Salsa でのプロジェクトページ]]
   - [[https://salsa.debian.org/tokyodebian-team/monthly-report][資料のgitレポジトリ]]
   - [[https://salsa.debian.org/tokyodebian-team/tokyodebian-team.pages.debian.net][ウェブページのgitレポジトリ]]
 - [[https://salsa.debian.org/tokyodebian-team/tokyodebian-team.pages.debian.net/-/blob/master/event-history.txt][勉強会参加者数の記録]]

* 過去の情報

 - [[hackcafe][不定期開催のハックカフェ]]
 - [[enkai][勉強会会場近くの宴会情報]]
 - [[meetingroom][比較的制限が少ない会議室の情報]]
