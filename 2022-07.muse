#title <lisp>year</lisp>年<lisp>month</lisp>月 東京エリア・関西合同Debian勉強会（オンライン開催）
<lisp>(progn (setq year 2022) (setq month 7) (setq day 18) (setq number 211) (setq eventid 251862) nil) </lisp>
* 開催概要

日時 | <lisp>year</lisp>年<lisp>month</lisp>月<lisp>day</lisp>日(月曜日、海の日) 19:00-20:00 (DebConf現地 12:00-13:00)
参加登録方法 | [[https://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass上での案内]]の掲示を見て参加申し込みをお願いします。<br>
開催方法 | インターネットを用いたオンラインのビデオ会議で行う予定です。インターネット回線がある場所からご参加ください。ビデオ会議の参加の方は[[https://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass上での案内]]をご参照ください。

* 当日のagenda



時間 || 内容 || 概要 || 発表者
19:00 (DebConf現地12:00) |  ビデオ会議のURLに集合 |  | 参加者全員
19:00-19:05 | 開会、説明事項の連絡 |  | dictoss さん
19:05-19:10 | 自己紹介、事前課題発表 |  | 参加者全員
19:10-19:55 | セミナー「DebConf22 コソボ現地レポート」 | ・Debian Projectでは、毎年DebConfというDebian開発者たちが集うイベントを開催しています。<br>・[[https://debconf22.debconf.org/][DebConf22]] はコソボのPrizrenにて、DebCampが7/10-7/16、DebConfが7/17-7/24の日程で開催されます。<br>・ **日本からDebian開発者の henrich さんが現地に赴き参加することになっており、現地の状況をレポートしていただけることになりました。** <br>・当日の現地予定はDebConf22 Day2の最中です。henrich さんが参加して感じていることをお話ししていただきつつ、勉強会に参加する皆様から聞いてみたいことを質問してみましょう。 | henrich さん
19:55-20:00 | 閉会、諸連絡 |  | dictoss さん
20:00 (DebConf現地13:00) | 解散 |  |

* 事前課題

 - ありません。

* 勉強会の申し込みと詳細情報

 - 勉強会の申し込みは イベント管理サービスである [[http://debianjp.connpass.com/event/<lisp>(format "%.6d" eventid)</lisp>/][connpass]] に掲示しています。詳細はリンク先を参照ください。

* 資料

 - [[./pdf<lisp>year</lisp>/debianmeetingresume<lisp>(format "%.4d%.2d" year month)</lisp>.pdf][PDF事前配布資料]]
 - [[./pdf<lisp>year</lisp>/debianmeetingresume<lisp>(format "%.4d%.2d" year month)</lisp>-presentation.pdf][PDF司会進行資料]]

* 参加報告

