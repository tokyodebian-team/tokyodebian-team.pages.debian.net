# BoF: 「xz-utilsのバックドア問題の情報交換」

## イベント概要

- イベント名
    - 東京エリアDebian勉強会 2024年4月度
    - https://tokyodebian-team.pages.debian.net/2024-04.html
- 開催日時
    - 2024-04-20(土) 14:00-16:00
- 場所
    - オンラインによるビデオ会議（Google Meet）
- 参加者（敬称略） 8 名
    - dictoss
    - kenhys
    - knok
    - su_do
    - yy_y_ja_jp
    - znz
    - sanadan
    - YukiharuYABUKI
- このドキュメントのライセンス
    - GPLv3

## BoFメモ「xz-utilsのバックドア問題の情報交換」


### 参考情報

- XZ Utilsに悪意のあるコードが挿入された問題（CVE-2024-3094）について
    - https://www.jpcert.or.jp/newsflash/2024040101.html
    - Debianの情報
        - https://security-tracker.debian.org/tracker/CVE-2024-3094
        - https://lists.debian.org/debian-security-announce/2024/msg00057.html

- 現在のxz-utilsパッケージ (Debian Package Tracking)
    - https://packages.qa.debian.org/x/xz-utils.html
    - stable       5.4.1-0.2 (影響なし)
    - testing      5.6.1+really5.4.5-1 (影響あり、バージョンを巻き戻し)
    - unstable   5.6.1+really5.4.5-1 (影響あり、バージョンを巻き戻し)
    - Ubuntu     5.6.1+really5.4.5-1 (影響あり、バージョンを巻き戻し)

- 開発中のUbuntu 24.04 LTSにも影響があり、ベータ版のリリースが1週間遅れた
    - https://gihyo.jp/admin/clip/01/ubuntu-topics/202404/05

- xz backdoor (2024-03-29)
    - https://lists.debian.org/debian-devel/2024/03/msg00333.html
    - 2024-02-28にunstableでreally5.4.5へ巻き戻し
        - https://tracker.debian.org/news/1515519/accepted-xz-utils-561really545-1-source-into-unstable/

- XZ Utilsにバックドア攻撃が行われるまでのタイムラインまとめ (2024-04-03の記事)
    - https://gigazine.net/news/20240403-timeline-of-xz-open-source-attack/
    - Debian unstableに問題のバージョンが混入したのは、2024-02-26
        - 混入直前：https://snapshot.debian.org/archive/debian/20240225T151757Z/pool/main/x/xz-utils/
        - 5.6.0混入直後：https://snapshot.debian.org/archive/debian/20240226T213049Z/pool/main/x/xz-utils/
        - really版リリース直後：https://snapshot.debian.org/archive/debian/20240328T211728Z/pool/main/x/xz-utils/

- 分析
    - https://research.swtch.com/xz-script 仕込まれたスクリプトの解析
    - https://boehs.org/node/everything-i-know-about-the-xz-backdoor
        - JiaT75 が xz 以前にも bsdtar に悪意あるコードをPRしていた

### 参加者の意見

- どういう脆弱性だったのか？
    - xz-utilsライブラリのパッケージが脆弱性を生み出しているが、このコードはsshのログイン認証を迂回するような細工を行うものだった
    - 問題となったコードはm4マクロだった
        - 読める人はあまりいない気がする

- debian.orgのunstableで今回戻したバージョンは、疑いがある人のコミットが1つもないところまで戻している
    - このメンテナーは信用できるのか？という感じで見られている
    - https://joeyh.name/blog/entry/reflections_on_distrusting_xz/
        - joeyh さんがそうすべきとした理由

- debianのバグレポートでもソーシャルハッキングのような感じで、バグ入りのバージョンを入れてほしいと圧力がかかっていた
    - https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1067708
    - debianも攻撃されたと言えると思う

- MiniDebConf ベルリンでこの脆弱性の話の枠がある模様
    - https://salsa.debian.org/ftp-team/xz-2024-incident/-/issues/12

- 今回の問題は人とコードの問題をわけて議論する必要があると思う

- 人の話
    - コミット権はどのような理由で誰に与えるかの問題
        - コミット権の与え方がゆるい人を作らない、というのはいいがその見極めは難しい気がする
        - debianでは公的機関が発行した証明書で本人認証をした上でDebian Developerを任命することにしている
    - ソーシャルハッキング
        - 事前に対応するのは難しい
        - 事後対応になりそうなため、どのように対処するか事前にある程度決めておく

- コードの話
    - 今回はtarballにマルウェアのコードが入り込んだが、gitにはマルウェアのコードはなかった
        - debian.orgではtarballとgitの中身をクロスチェックするツールをつくってはどうかという意見あり
    - Chain of Responsibilityという考え方
    - コードを作る人 (upstream) と送り届ける人 (distributor) に分けて考えたほうがいい
        - debianはdistributor
    - コードのレビューで対応する案もあるが、この対応をボランティアでやりきるのはかなり辛い
        - 実際見抜けるのか、という問題もありそう
    - 今回の脆弱性はm4マクロで埋め込んだものであるが、これは技術的に古く面倒見れる人が少ない
        - autoconf、automakeはビルドシステム職人が行う領域な気がする
        - 新しいビルドシステム形式に置き換えを進めるのがいいのか？
        - 辛くないビルドシステムとは何？
            - なにかあるかと言われると困る。どれも辛い気がする
            - スクリプト言語などのビルドしないソースコードが主流になればよい未来が来る？
        - 標準技術は時代とともに変わる
            - https://www.explainxkcd.com/wiki/index.php/927:_Standards
    - 多様な環境を提供するのがDebianのアイデンティティだと思う
    - 対応は個人でできるレベルを超えているため、組織的に関与していかないと対応は難しいと思う
